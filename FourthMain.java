public class FourthMain {
 // Напишите метод, заменяющий в произвольной строке все вхождения слова «блин» на «вырезано цензурой».

         public static void main(String[] args) {
         String s1 = "блин, опять упал.";
         System.out.println("Исходное предложение: " + s1);
         stringMethod(s1);
         }
         private static void stringMethod(String s1) {
         String s2 = s1.replaceAll("блин", "вырезано цензурой");
         System.out.println("Изменено на: " + s2);
         }
}